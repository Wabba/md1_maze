package assign01;


/**
 * Assignment 01 - Implement parallel maze solving algorithm using JAVA Concurrent API
 * 
 * use maze.isNorth(x,y), maze.isSouth(x,y), etc to check if there is wall in exact direction
 * use maze.setVisited(x,y) and maze.isVisited(x,y) to check is you already have been at particular cell in the maze
 * @author Janis
 */
public class MazeSolver {
    
    Maze maze;
    
    //Current position
    int x;
    int y;
    
    public MazeSolver(Maze m){
        this.maze = m;
    }
    
    /**
     * Solve maze starting from initial positions
     * @param x starting position in x direction
     * @param y starting position in y direction
     */
    public void solveMaze(int x, int y){
        //Set current position to initial position
        this.x = x;
        this.y = y;
        // TODO Your algorithm here. If necessary, create another methods (for example, recursive solve method)
       
        solve(this.x, this.y);
    }
    
    
    public void solve(int x, int y) {
    	Runnable threadManager = new ThreadManager();
    	
    	this.x = x;
    	this.y = y;
    	
    	boolean deadEnd = false;
    	
    	
    	
    	
    	
    	while (deadEnd == false) {
    		boolean[] openPaths = {false, false, false, false};
    		int viablePaths = 0;
    		
    		for(int i = 0; i < 4; i++) {
    			if(!maze.isEast(this.x, this.y) && !maze.isVisited(this.x+1, this.y)) {
    				openPaths[0] = true;
    			}
    			if(!maze.isNorth(this.x, this.y) && !maze.isVisited(this.x, this.y+1)) {
    				openPaths[1] = true;
    			}
    			if(!maze.isWest(this.x, this.y) && !maze.isVisited(this.x-1, this.y)) {
    				openPaths[2] = true;
    			}
    			if(!maze.isSouth(this.x, this.y) && !maze.isVisited(this.x, this.y-1)) {
    				openPaths[3] = true;
    			}
    		}
    		
    		for (boolean path: openPaths) {
    			if(path == true) {
    				viablePaths += 1;
    			}
    		}
    		
    		if(viablePaths == 1) {
            	if(openPaths[0] == true) {
            		maze.setVisited(this.x, this.y);
            		this.x += 1;
            		System.out.println("East: " + this.x + " " + this.y);
            		
            		StdDraw.setPenColor(StdDraw.BLUE);
                    StdDraw.filledCircle(this.x + 0.5, this.y + 0.5, 0.25);
                    //sleeps for 30 ms
                    StdDraw.show(30);
            	}
            	if(openPaths[1] == true) {
            		maze.setVisited(this.x, this.y);
            		this.y += 1;
            		System.out.println("North: " + this.x + " " + this.y);
            		
            		StdDraw.setPenColor(StdDraw.BLUE);
                    StdDraw.filledCircle(this.x + 0.5, this.y + 0.5, 0.25);
                    //sleeps for 30 ms
                    StdDraw.show(30);
            		
            	}
            	if(openPaths[2] == true) {
            		maze.setVisited(this.x, this.y);
            		this.x -= 1;
            		System.out.println("West: " + this.x + " " + this.y);
            		
            		StdDraw.setPenColor(StdDraw.BLUE);
                    StdDraw.filledCircle(this.x + 0.5, this.y + 0.5, 0.25);
                    //sleeps for 30 ms
                    StdDraw.show(30);
            		
            	}
            	if(openPaths[3] == true) {
            		maze.setVisited(this.x, this.y);
            		this.y -= 1;
            		System.out.println("South: " + this.x + " " + this.y);
            		
            		StdDraw.setPenColor(StdDraw.BLUE);
                    StdDraw.filledCircle(this.x + 0.5, this.y + 0.5, 0.25);
                    //sleeps for 30 ms
                    StdDraw.show(30);
            		
            	}
    		}else if(viablePaths == 0) {
    			deadEnd = true;
    		}else {
    			if(openPaths[0] == true) {
    				maze.setVisited(this.x, this.y);
            		this.x += 1;
            		System.out.println("East T: " + this.x + " " + this.y);
            		
            		StdDraw.setPenColor(StdDraw.BLUE);
                    StdDraw.filledCircle(this.x + 0.5, this.y + 0.5, 0.25);
                    //sleeps for 30 ms
                    StdDraw.show(30);
            		
    			    Thread thread0 = new Thread(threadManager);
    				thread0.start();
    				solve(this.x, this.y);
    				try {
    					thread0.join();
    				} catch (InterruptedException e) {
    					e.printStackTrace();
    				}
    				System.out.println("Thread state: " + thread0.getState());
    			}
    			if(openPaths[1] == true) {
    				maze.setVisited(this.x, this.y);
            		this.y += 1;
            		System.out.println("North T: " + this.x + " " + this.y);
            		
            		StdDraw.setPenColor(StdDraw.BLUE);
                    StdDraw.filledCircle(this.x + 0.5, this.y + 0.5, 0.25);
                    //sleeps for 30 ms
                    StdDraw.show(30);
            		
    				Thread thread1 = new Thread(threadManager);
    				thread1.start();
    				solve(this.x, this.y);
    				try {
    					thread1.join();
    				} catch (InterruptedException e) {
    					e.printStackTrace();
    				}
    				System.out.println("Thread state: " + thread1.getState());
    			}
    			if(openPaths[2] == true) {
    				maze.setVisited(this.x, this.y);
            		this.x -= 1;
            		System.out.println("West T: " + this.x + " " + this.y);
            		
            		StdDraw.setPenColor(StdDraw.BLUE);
                    StdDraw.filledCircle(this.x + 0.5, this.y + 0.5, 0.25);
                    //sleeps for 30 ms
                    StdDraw.show(30);
            		
    				Thread thread2 = new Thread(threadManager);
    				thread2.start();
    				solve(this.x, this.y);
    				try {
    					thread2.join();
    				} catch (InterruptedException e) {
    					e.printStackTrace();
    				}
    				System.out.println("Thread state: " + thread2.getState());
    			}
    			if(openPaths[3] == true) {
    				maze.setVisited(this.x, this.y);
            		this.y -= 1;
            		System.out.println("South T: " + this.x + " " + this.y);
            		
            		StdDraw.setPenColor(StdDraw.BLUE);
                    StdDraw.filledCircle(this.x + 0.5, this.y + 0.5, 0.25);
                    //sleeps for 30 ms
                    StdDraw.show(30);
            		
    				Thread thread3 = new Thread(threadManager);
    				thread3.start();
    				solve(this.x, this.y);
    				try {
    					thread3.join();
    				} catch (InterruptedException e) {
    					e.printStackTrace();
    				}
    				System.out.println("Thread state: " + thread3.getState());
    			}
    			
    		}
    		
		}
        	
    }
    	
    }
    
    
    
    
    
    

